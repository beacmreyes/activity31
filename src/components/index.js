import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import Course from '../components/Course'

export{
	Banner,
	Highlights,
	Course
}