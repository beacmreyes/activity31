import React,{useState, useEffect} from 'react';
import PropTypes from 'prop-types'
import { Card,Button } from 'react-bootstrap'


export default function Course({courseProp: {name,description,price, start_date, end_date}}){
	// const {name,description,price, start_date, end_date}=courseProp; //destructure the object

	const [enrollees, setEnrollees]=useState(0);
	const [seats, setSeats]=useState(3);
	const [isOpen, setIsOpen]=useState(true);

	function enroll(){
		setEnrollees(enrollees+1);
		setSeats(seats-1);
		console.log('Enrollees:' + enrollees);
		console.log('Seats:' + seats);

		//syncronous kaya sabay na nirarun yung setCount at console.log.
	}

		// function button(){
		// 	if(isOpen){
		// 		return(<Button className="bg-primary" onClick={enroll} >Enroll</Button>)
		// 	}else{
		// 	return(<Button className="bg-danger" onClick={enroll} disabled >Not Available</Button>)
		// 	}
		// }

	// useEffect(()=>{
	// 	code block goes here
	// },[states to trigger useEffect])

	useEffect(()=>{
		if (seats===0){
			setIsOpen(false)
		}
	},[seats])

	return(
		<Card className = "cardHighlight">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Text>
					<span>Description:</span>
					<br />
					<span>{description}</span>
					<br />
					<span>Start Date: </span>
					{start_date}
					<br />
					<span>End Date: </span>
					{end_date}
					<br />
					{
						isOpen?
						<Button className="bg-primary" onClick={enroll} >Enroll</Button>
						:
						<Button className="bg-danger" onClick={enroll} disabled >Not Available</Button>
					// button()
					}
				</Card.Text>
			</Card.Body>
		</Card>
	)


}

//checks if the Course component is getting the correct prop type/data structure
//used only for validation
Course.propTypes = {
	//shape() is used to check that a prop opbject conforms to a specified shape
	courseProp: PropTypes.shape({
		name:PropTypes.string.isRequired,
		description:PropTypes.string.isRequired,
		price:PropTypes.number.isRequired

	})
}

//propTypes<-component of course. 
//Proptypes<- call yung inimport