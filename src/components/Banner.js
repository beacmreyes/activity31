import React from 'react';
// import Jummbotron from 'react-bootstrap/Jumbotron'
// import Button from 'react-bootstrap/Button'
import { Jumbotron, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Banner({data}){
	const {title, content, destination, label} =data

	return(
		<Row>
			<Col>
				<Jumbotron>
					<h1>{title}</h1>
					<p>{content}</p>
					<Link to={destination}>
						{label}
					</Link>		
				</Jumbotron>
			</Col>
		</Row>

	)

}