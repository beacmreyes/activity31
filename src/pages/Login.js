import React, { useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

import UserContext from '../UserContext';
import Users from '../data/users';

export default function Login() {
	const { user, setUser} = useContext(UserContext);


	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [willRedirect, setWillRedirect] = useState(false)

	function authenticate(e) {
	    //prevent redirection via form submission
	    e.preventDefault();

	 //    if (email !== "johndoe@gmail.com"){
		// 	console.log("Incorrect username and password");
		// } else {
		// 	console.log(`${email} has been verified! Welcome back!`)
		// 	//clear input fields after submission
		//     setEmail('');
		//     setPassword('');
		// }

		console.log(`${email} has been verified! Welcome back!`)
		//clear input fields after submission
		// setEmail('');
		// setPassword('');

		const match = Users.find(user =>{
			return (user.email === email && user.password === password);

		})

		console.log(match)
		if(match){
		 	localStorage.setItem('email', email);
		 	localStorage.setItem('isAdmin', match.isAdmin);

		 	setUser({
		 		email: localStorage.getItem('email'),
		 		isAdmin: match.isAdmin
		 	})
		 	setWillRedirect(true);
		} else {
			console.log('Authentication failed, no match found')
		}
	    
	setEmail('');
	setPassword('');
	}



	useEffect(() => {
		if((email !== '' && password !== '')){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ email, password ])

	return(
		willRedirect === true
		?
		<Redirect to='/courses'/>
		:
		<Form onSubmit={e => authenticate(e)}>
		    <Form.Group controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		            type="email" 
		            placeholder="Enter email" 
		            value={email}
		        	onChange={(e) => setEmail(e.target.value)}
		            required
		        />
		    </Form.Group>

		    <Form.Group controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		            type="password" 
		            placeholder="Password" 
		            value={password}
		        	onChange={(e) => setPassword(e.target.value)}
		            required
		        />
		    </Form.Group>

		    { isActive
				? 
				<Button className="bg-primary" type="submit">
					Submit
				</Button>
				:
				<Button className="bg-danger" type="submit" disabled>
					Submit
				</Button>
			}

		</Form>
	)
}
