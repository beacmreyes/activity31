import React, { Fragment } from 'react';
import { Link } from 'react-router-dom'
import { Jumbotron } from 'react-bootstrap'

export default function PageNotFound (){

	// const data = {
	// 	title: "404 Not found",
	// 	content: "The page you are looking for cannot be found",
	// 	destination:"/",
	// 	label:"Back home"
	// }
	
	return (
		// <Banner />
		<Fragment>
			<Jumbotron>
				<h1>404 Page Not Found</h1>
				<p>The page you are looking for cannot be found.</p>
      	    	<Link to="/" className="nav-link">Return to Home</Link>	
			</Jumbotron>

		</Fragment>
	)
}