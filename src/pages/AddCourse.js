import React, { useState, useEffect, useContext} from 'react';
import { Form,FormGroup , Button,Col } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

import UserContext from '../UserContext';
import Users from '../data/users';

export default function AddCourse() {
	const { user, setUser} = useContext(UserContext);

	const [id, setId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');



	function submitCourse(e) {

			e.preventDefault();

			console.log(`${name} with ID: ${id} is set to start on ${startDate} for PhP ${price} per slot`)
		setId('');
		setName('');
		setDescription('');
		setPrice('');
		setStartDate('');
		setEndDate('');
	}


	// useEffect(() => {
	// 	if((id !== '' && name !== '')){

	// 	}
	// }, [ id,name,description,price,startDate,endDate ])


	return(
		<Form onSubmit={e => submitCourse(e)}>
		    <Form.Group row controlId="id">
		        <Form.Label>Course ID: </Form.Label>
			        <Form.Control 
			            type="string" 
			            value={id}
			            onChange={(e) => setId(e.target.value)}
			            placeholder="Enter Course ID"
			            required
			        />
		    </Form.Group>

		    <Form.Group row controlId="id">
		        <Form.Label>Course Name: </Form.Label>
			        <Form.Control 
			            type="string" 
			            value={name}
			            onChange={(e) => setName(e.target.value)}
			            placeholder="Enter CourseName "
			            required
			        />
		    </Form.Group>

		    <Form.Group row controlId="id">
		        <Form.Label>Description: </Form.Label>
			        <Form.Control 
			            as="textarea" 
			            value={description}
			            onChange={(e) => setDescription(e.target.value)}
			            placeholder="Enter Course Description"
			            required
			        />
		    </Form.Group>

		    <Form.Group row controlId="id">
		        <Form.Label>Price: </Form.Label>
			        <Form.Control 
			            type="string" 
			            value={price}
			            onChange={(e) => setPrice(e.target.value)}
			            placeholder="0"
			            required
			        />
		    </Form.Group>

		    <Form.Group row controlId="id">
		        <Form.Label>Start Date: </Form.Label>
			        <Form.Control 
			            type="date" 
			            value={startDate}
			            onChange={(e) => setStartDate(e.target.value)}
			            required
			        />
		    </Form.Group>


		    <Form.Group row controlId="id">
		        <Form.Label>End Date: </Form.Label>
			        <Form.Control 
			            type="date" 
			            value={endDate}
			            onChange={(e) => setEndDate(e.target.value)}
			            required
			        />
		    </Form.Group>


			<Button className="bg-primary" type="submit">
				Submit
			</Button>


		</Form>
	)
}
