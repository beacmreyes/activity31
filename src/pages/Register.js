import React, {useState, useEffect } from 'react'
import {Form, Button} from 'react-bootstrap'

export default function Register() {

	const[email, setEmail] = useState('');
	const[password1, setPassword1] = useState('');
	const[password2, setPassword2] = useState('');
	//state to determine wheter submit button is enabled or not 
	const[isActive, setIsActive] = useState(false);

	// console.log(email)
	// console.log(password1)
	// console.log(password2)

	function registerUser(e) {
		e.preventDefault();

		//clear input fields
		setEmail("");
		setPassword1("");
		setPassword2("");

		console.log("Thank you for registering!")
	}

	useEffect(()=>{
		//validation to enable submit button when all fields are populatd and passwords match
		if((email !=='' && password1!=='' && password2!=='') &&(password1===password2)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password1,password2])

	return(
		<React.Fragment>
			<Form onSubmit = {e => registerUser(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter Password"
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>
				{/*conditionally render submit button based on isActive state*/}
				{ isActive
					?

					<Button className="bg-primary" type="submit" id="submitBtn">
						Submit
					</Button>
					:
					<Button className="bg-danger" type="submit" id="submitBtn" disabled>
						Submit
					</Button>				
				}
			</Form>
		</React.Fragment>
	)
}