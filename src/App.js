// import logo from './logo.svg';
import React, { useState } from 'react'
import{ Container } from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'


import './App.css';
import NavBar from './components/NavBar'
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import AddCourse from './pages/AddCourse'
import Error from './pages/Error';
import { UserProvider } from './UserContext'

function App() {

	const [user, setUser] = useState({
		email: localStorage.getItem('email'),
		isAdmin: localStorage.getItem('isAdmin')==='true'
	})

	const unsetUser =()=>{
		localStorage.clear()

		setUser({
			email: null,
			isAdmin: null
		})
	}


  return (
    <React.Fragment>
    	<UserProvider value ={{user,setUser, unsetUser}}>
	    	<Router>
		    	<NavBar />,
		    	<Container>
		    		<Switch >
		    			<Route exact path="/" component={Home} />
		    			<Route exact path="/courses" component={Courses} />
		    			<Route exact path="/login" component={Login} />
		    			<Route exact path="/register" component={Register} />
		    			<Route exact path="/logout" component={Logout} />
		    			{(user.isAdmin)
						?
		    				<Route exact path="/addcourse" component={AddCourse} />
		    			:
		    				null
		    			}
		    			<Route component={Error}/>
		    		</Switch>
		    	</Container>
		    </Router>
		</UserProvider>
    </React.Fragment>
  );

}

export default App;
  